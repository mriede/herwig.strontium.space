import { createStore } from 'vuex'

export default createStore({
  state: {
  	nav: {
  		enableTransparency: true,
  		isTransparent: true,
      enableMenu: false,
  	},
  },
  mutations: {
  	SET_NAV_TRANSPARENCY_ENABLED (state, isEnabled) {
  		state.nav.enableTransparency = isEnabled
  	},

  	SET_NAV_TRANSPARENT (state, isTransparent) {
  		state.nav.isTransparent = isTransparent
  	},

    SWITCH_MENU (state) {
      state.nav.enableMenu = !state.nav.enableMenu
    }
  },
  getters: {
  	isNavTransparencyEnabled: state => state.nav.enableTransparency,
  	isNavTransparent: 		    state => state.nav.isTransparent && state.nav.enableTransparency,
    isMenuEnabled:            state => state.nav.enableMenu,
  },
  actions: {
  },
  modules: {
  }
})
