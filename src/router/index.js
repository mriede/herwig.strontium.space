import { createRouter, createWebHistory } from 'vue-router'

import Home from '../views/Home.vue'
import UntersuchungenOld from '../views/UntersuchungenOld.vue'
import Untersuchungen from '../views/Untersuchungen.vue'
import Mammographie from '../views/Mammographie.vue'
import Befundung from '../views/Befundung.vue'
import Anmeldung from '../views/Anmeldung.vue'
import Kontakt from '../views/Kontakt.vue'
import Login from '../views/Login.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/de/untersuchungen',
    name: 'Untersuchungen',
    component: Untersuchungen,
  },
  {
    path: '/de/mammographie',
    name: 'Mammographie',
    component: Mammographie,
  },  
  {
    path: '/de/befundung',
    name: 'Befundung',
    component: Befundung,
  },  
  {
    path: '/de/anmeldung',
    name: 'Anmeldung',
    component: Anmeldung,
  },
  {
    path: '/de/kontakt',
    name: 'Kontakt',
    component: Kontakt,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  window.scrollTo(0, 0)

  next()


  //if (to.name !== 'Login' && !store.getters.isAuthenticated) next({ name: 'Login' })
  //else next()
})

export default router
